﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollector : MonoBehaviour
{


    int _numberOfCherries = 0;

    [SerializeField]
    private Text cherriesText;

   
    [SerializeField]
    private AudioSource _audioSource;

  
    private void OnTriggerEnter2D(Collider2D collision)
    {

        
        if(collision.gameObject.CompareTag("Stawberry"))
        {
            _audioSource.Play();
            _numberOfCherries = _numberOfCherries +  10;
            Destroy(collision.gameObject);
            updateScore(_numberOfCherries);
        }
    }

    private void updateScore(int _score)
    {
        cherriesText.text = "Score : "+ _score;
    }
}
