﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawRotatioan : MonoBehaviour
{
    [SerializeField]
    private float _speed = 2.0f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 360 * _speed * Time.deltaTime);
        
    }
}
