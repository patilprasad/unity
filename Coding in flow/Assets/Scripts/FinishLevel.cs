﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLevel : MonoBehaviour
{
    private AudioSource _audioSource;
    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        if(_audioSource == null)
        {
            Debug.Log("Failed to get FinishLevel::_audioSource");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Main_Player"))
        {
            _audioSource.Play();
            Invoke("ComplateLeve", 1.0f);
           
        }
    }

    private void ComplateLeve()
    {
        int y = SceneManager.GetActiveScene().buildIndex;
        y++;
        SceneManager.LoadScene(y);
    }
}
