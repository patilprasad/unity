﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    private float directioanX;
    private Animator _animatioan;
    private SpriteRenderer _spriteRenderer;
    private BoxCollider2D _boxCollider2D;

    [SerializeField]
    private float moveSpeed = 7.0f;

    [SerializeField]
    private float jumpForce = 14.0f;

    [SerializeField]
    private LayerMask _groundLayerMask;

    [SerializeField]
    private AudioSource _audioSource;

    private enum MovementState { idle,running,jumping,falling}; 
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        if(rigidBody == null)
        {
            Debug.Log("Get To Failed Rigidbody");
        }
        _animatioan = GetComponent<Animator>();

        if (_animatioan == null)
        {
            Debug.Log("Get To Failed Animator");
        }

        _spriteRenderer = GetComponent<SpriteRenderer>();

        if(_spriteRenderer == null)
        {
            Debug.Log("Get To Failed SpriteRenderer");
        }

        _boxCollider2D = GetComponent<BoxCollider2D>();

        if(_boxCollider2D == null)
        {
            Debug.Log("Get To Failed BoxCollider2D");
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        directioanX = Input.GetAxis("Horizontal");
        //directionX recvives 1 when its in right directioan and recvices -1 in left directioan
        //Therefore we multiplay with speed

        rigidBody.velocity = new Vector2(directioanX * moveSpeed, rigidBody.velocity.y );

        FlipAxis(directioanX);

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            _audioSource.Play();
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, jumpForce, 0.0f);
        }

        SwitchAnimatioan();
    }

    private void SwitchAnimatioan()
    {
        MovementState state;
        if (directioanX < 0.0f)
        {
            state = MovementState.running;
        }
        else if (directioanX > 0.0f)
        {
            state = MovementState.running;
        }
        else
        {
            state = MovementState.idle;
        }

        if(rigidBody.velocity.y > 0.1f)
        {
            state = MovementState.jumping;
        }
        else if(rigidBody.velocity.y < -0.1f)
        {
            state = MovementState.falling;
        }
            
        _animatioan.SetInteger("state", (int)state);
    }

    private void FlipAxis(float dir)
    {
        if(dir < 0)
        {
            _spriteRenderer.flipX = true;
        }
        else
        {
            _spriteRenderer.flipX = false;
        }
    }

    private bool IsGrounded()
    {
       return Physics2D.BoxCast(_boxCollider2D.bounds.center, _boxCollider2D.bounds.size, 0.0f, Vector2.down, 0.1f,_groundLayerMask);
    }
}
