﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    private Animator _animator;
    private Rigidbody2D _rigidbody2D;
    private CameraController _cameraController;

    [SerializeField]
    private AudioSource _audioSource;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        if(_animator == null)
        {
            Debug.Log("Failed To Get PlayerLife::_animator");
        }

        _rigidbody2D = GetComponent<Rigidbody2D>();
        if(_rigidbody2D == null)
        {
            Debug.Log("Failed To Get PlayerLife::_rigidbody2D");
        }

        _cameraController = GameObject.Find("Main_Camera").GetComponent<CameraController>();
        if (_cameraController == null)
        {
            Debug.Log("Failed To Get PlayerLife::_cameraController");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Spikes"))
        {
            Die();
        }
    }

    private void Die()
    {
        _rigidbody2D.bodyType = RigidbodyType2D.Static;
        _audioSource.Play();
        _animator.SetTrigger("death");
        Destroy(_cameraController);
        Destroy(this.gameObject, 1.1f);

    }

    private void RestartLevel()
    {
        int y = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(y);
    }
    
}
