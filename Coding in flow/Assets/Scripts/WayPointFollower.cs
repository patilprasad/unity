﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointFollower : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _wayPoints;

    private int _currentWayPointIndex = 0;
    private int _currentWayPointIndexRev = 1;

    [SerializeField]
    private float _speedOfPlatform = 2.0f;

    [SerializeField]
    private int id;

    // Update is called once per frame
    void Update()
    {
        //This logics is used to decide which waypoint we want to choose

        if (id == 0)
        {
            if (Vector2.Distance(_wayPoints[_currentWayPointIndex].transform.position, transform.position) < 0.1f)
            {
                _currentWayPointIndex++;
                if (_currentWayPointIndex >= _wayPoints.Length)
                {
                    _currentWayPointIndex = 0;
                }
            }
            transform.position = Vector2.MoveTowards(transform.position, _wayPoints[_currentWayPointIndex].transform.position, Time.deltaTime * _speedOfPlatform);
        }
        else if(id == 1)
        {
            
            if (Vector2.Distance(_wayPoints[_currentWayPointIndexRev].transform.position, transform.position) < 0.1f)
            {
                _currentWayPointIndexRev++;
                if (_currentWayPointIndexRev >= _wayPoints.Length)
                {
                    _currentWayPointIndexRev = 0;
                }
            }
            transform.position = Vector2.MoveTowards(transform.position, _wayPoints[_currentWayPointIndexRev].transform.position, Time.deltaTime * _speedOfPlatform);
        }
       
       
    }


}
