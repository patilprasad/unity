﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSticky : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //We want player folows the platform
        //in collision we get access of Player
        //So we Set player as a child to platform

        if (collision.gameObject.CompareTag("Main_Player"))
        {
            collision.gameObject.transform.SetParent(transform);

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Main_Player"))
        {
            collision.gameObject.transform.SetParent(null);

        }
    }
}

